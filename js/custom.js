$("#button1").click(function () {

	console.log('clicked');

	$('#try-1').toggleClass('description-animation')

});

$("#button2").click(function () {

	var test = $('#test span');

	console.log("button 2 clicked");

	function logtoconsole() {
		console.log("hello");
	}

	function addtoClass() {
		setTimeout(function () {
			test.addClass('title-animation');
		},2000)
	}

	setTimeout(function () {

		for (var i = 0; i <= test.length; i++) {

			logtoconsole();
			addtoClass()

		}

	}, 3000);
});


$(".header-background .pages-title-animation h1").each(function () {
	var $t = $(this),
		rows = $.trim($t.html()).split("<br>");

	$t.html("");

	$.each(rows, function (i, val) {
		$('<span class="row"></span>').appendTo($t);

		var letters = $.trim(val).split("");

		$.each(letters, function (j, v) {
			v = v == " " ? "&nbsp;" : v;
			$("<span>" + $.trim(v) + "</span>").appendTo($(".row:last", $t));
		});
	});
});

function addtitleAnimations($xs) {
	for (i = 0; i < $xs.find('.pages-title-animation h1 span').length; i++) {
		(function (ind) {
			setTimeout(function () {
				$xs.find('.pages-title-animation h1 span span').eq(ind).addClass('animateTitle');
			}, ind * 150);
		})(i);
	}

}